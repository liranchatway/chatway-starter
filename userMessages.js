module.exports = {
  messages: {
    NO_DIRECTORY_ARUGMENT:
      'Please provide the name of the directory you want to create as an argument.',
    PROJECT_CLONING: 'Cloning Chatway Project...',
    CHECKING_BRANCH: 'checking branch liran/cli-tool',
    INSTALLING_NPM: 'Running npm install...',
    REMOVE_IOS: 'Removing Ios Platform... ',
    REMOVE_ANDROID: 'removing Android Platform',
    ADD_ANDROID: 'Adding Android...',
    ADD_IOS: 'Add Ios...',
    JAVA_DISCARD: 'Discard all java changes',
    PLIST_DISCRD: 'Discard chatway-info.plist',
    MANIFEST_DISCARD: 'Discard AndroidManifest.xml',
    GOOGLE_SERVICES_DISCARD: 'Discard google-services.json',
    DISCARD_OTHERS: 'Discard Various Files',
    KEYBOARD_IOS: 'ADDING IOS KEYBOARD PLUGIN',
    KEYBOARD_ANDROID: 'ADDING ANDROID KEYBOARD PLUGIN',
    DEVICE_PLUGIN_IOS: 'ADDING cordova-plugin-device FOR IOS',
    DEVICE_PLUGIN_ANDROID: 'ADDING cordova-plugin-device FOR ANDROID',
    LOCAL_NOTIFICATIONS: 'ADDING LOCAL NOTIFICATION TO ANDROID',
    INTENT_PLUGIN: 'ADDING PLUGIN INTENT TO ANDROID',
    PUSH_ANDROID: 'ADDING phonegap-plugin-push@2.1.3 TO ANDROID',
    PUSH_IOS: 'ADDING phonegap-plugin-push@1.11.1 TO IOS',
    MEDIA_PLUGIN: 'Adding cordova-plugin-media',
    DOWNLOAD_MANAGER_PLUGIN: 'ADDING downloadmanager plugin',
    DOWNLOAD_APP_MINIMIZE: 'Adding appminimize plugin',
    GENERATE_PRODUCTION_BUILD: 'Generating Production Build'
  },
  categories: {
    NPM: 'NPM',
    GIT: 'GIT',
    CORDOVA: 'CORDOVA',
    PLUGMAN: 'PLUGMAN',
    WEBPACK: 'webpack'
  }
};
