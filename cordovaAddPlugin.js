const commandExecuter = require('./commandExecuter');
const messages = require('./userMessages');

module.exports = function(options) {
  const { pluginName, workingPath, message } = options;
  return commandExecuter({
    command: 'cordova',
    commandArgs: ['plugin', 'add', pluginName, '--force'],
    cwd: workingPath,
    message,
    category: messages.categories.CORDOVA
  });
};
