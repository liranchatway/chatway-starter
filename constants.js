const BOLD_WITH_BG_THEME = 'BACKGROUND_1';
const NORMAL_WITH_BG_THEME = 'BACKGROUND_2';
const SPECIAL_COLOR_THEME = 'SPECIAL_COLOR_THEME';
const ERROR_THEME = 'ERROR';
const gitServer =
  'https://liranchatway@bitbucket.org/shaharChatway/chatway-client.git';

module.exports = {
  themes: {
    BOLD_WITH_BG_THEME,
    NORMAL_WITH_BG_THEME,
    SPECIAL_COLOR_THEME,
    ERROR_THEME,
  },
  gitServer
};
