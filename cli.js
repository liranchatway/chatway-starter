#! /usr/bin/env node

/** global imports */
const path = require('path');
const chalk = require('chalk');

/** local imports */
const { log } = console;
const version = require('./package.json').version;
const commandExecuter = require('./commandExecuter');
const cordovaPluginAdder = require('./cordovaAddPlugin');
const plugmanAdder = require('./plugmanAdder');
const { themes, gitServer } = require('./constants.js');
const { messages, categories } = require('./userMessages');
const styler = require('./styler');
const moment = require('moment');
const startTime = moment();

/** get and print the version. */
const versionTitle = `Chatway-starter Version: ${version}`;
log(styler(versionTitle, themes.SPECIAL_COLOR_THEME));

/** get arugments */
const [, , ...args] = process.argv;
const directoryName = args[0];

/** validte arguments */
if (!directoryName) {
  log(
    styler(
      'No Arguments Error. You have not specified a folder name',
      themes.ERROR_THEME
    )
  );
  process.exit();
}
/** get working path - the path where most of the commands will be executed. */
const workingPath = path.resolve('.', directoryName);

commandExecuter({
  command: 'git',
  commandArgs: ['clone', gitServer, workingPath],
  cwd: __dirname,
  message: messages.PROJECT_CLONING,
  category: categories.GIT
})
  .then(() => {
    return commandExecuter({
      command: 'npm',
      commandArgs: ['install'],
      cwd: workingPath,
      message: messages.INSTALLING_NPM,
      category: categories.NPM
    });
  })
  .then(() => {
    return commandExecuter({
      command: 'cordova',
      commandArgs: ['platform', 'remove', 'ios'],
      cwd: workingPath,
      message: messages.REMOVE_IOS,
      category: categories.CORDOVA
    });
  })
  .then(() => {
    return commandExecuter({
      command: 'cordova',
      commandArgs: ['platform', 'remove', 'android'],
      cwd: workingPath,
      message: messages.REMOVE_ANDROID,
      category: categories.CORDOVA
    });
  })
  .then(() => {
    return commandExecuter({
      command: 'cordova',
      commandArgs: ['platform', 'add', 'android'],
      cwd: workingPath,
      message: messages.ADD_ANDROID,
      category: categories.CORDOVA
    });
  })
  .then(() => {
    return commandExecuter({
      command: 'cordova',
      commandArgs: ['platform', 'add', 'ios'],
      cwd: workingPath,
      message: messages.ADD_IOS,
      category: categories.CORDOVA
    });
  })
  .then(() => {
    return plugmanAdder({
      platform: 'ios',
      pluginName: 'cordova-plugin-keyboard',
      workingPath,
      finishMessage: messages.KEYBOARD_IOS
    });
  })
  .then(() => {
    return plugmanAdder({
      platform: 'android',
      pluginName: 'cordova-plugin-ionic-keyboard',
      workingPath,
      finishMessage: messages.KEYBOARD_IOS
    });
  })
  .then(() => {
    return plugmanAdder({
      platform: 'android',
      pluginName: 'cordova-plugin-device',
      workingPath,
      finishMessage: messages.DEVICE_PLUGIN_ANDROID
    });
  })
  .then(() => {
    return plugmanAdder({
      platform: 'ios',
      pluginName: 'cordova-plugin-device',
      workingPath,
      finishMessage: messages.DEVICE_PLUGIN_IOS
    });
  })
  .then(() => {
    return plugmanAdder({
      platform: 'android',
      pluginName: 'cordova-plugin-local-notification',
      workingPath,
      finishMessage: messages.LOCAL_NOTIFICATIONS
    });
  })
  .then(() => {
    return plugmanAdder({
      platform: 'android',
      pluginName: 'https://github.com/napolitano/cordova-plugin-intent.git',
      workingPath,
      finishMessage: messages.INTENT_PLUGIN
    });
  })
  .then(() => {
    return plugmanAdder({
      platform: 'android',
      pluginName: 'phonegap-plugin-push@2.1.3',
      workingPath,
      finishMessage: messages.PUSH_ANDROID
    });
  })
  .then(() => {
    return plugmanAdder({
      platform: 'ios',
      pluginName: 'phonegap-plugin-push@1.11.1',
      workingPath,
      finishMessage: messages.PUSH_IOS
    });
  })
  .then(() => {
    return commandExecuter({
      command: 'git',
      commandArgs: ['checkout', '*.java'],
      cwd: workingPath,
      message: messages.JAVA_DISCARD,
      category: categories.GIT
    });
  })
  .then(() => {
    return commandExecuter({
      command: 'git',
      commandArgs: ['checkout', 'platforms/ios/chatway/chatway-Info.plist'],
      cwd: workingPath,
      message: messages.PLIST_DISCARD,
      category: categories.GIT
    });
  })
  .then(() => {
    return commandExecuter({
      command: 'git',
      commandArgs: ['checkout', 'platforms/android/AndroidManifest.xml'],
      cwd: workingPath,
      message: messages.MANIFEST_DISCARD,
      category: categories.GIT
    });
  })
  .then(() => {
    return commandExecuter({
      command: 'git',
      commandArgs: ['checkout', 'platforms/android/google-services.json'],
      cwd: workingPath,
      message: messages.GOOGLE_SERVICES_DISCARD,
      category: categories.GIT
    });
  })
  .then(() => {
    return commandExecuter({
      command: 'git',
      commandArgs: [
        'checkout',
        'platforms/android/project.properties',
        'platforms/android/build.gradle',
        'platforms/android/project.properties'
      ],
      cwd: workingPath,
      message: messages.DISCARD_OTHERS,
      category: categories.GIT
    });
  })
  .then(() => {
    return cordovaPluginAdder({
      pluginName: 'cordova-plugin-media',
      message: messages.MEDIA_PLUGIN,
      workingPath
    });
  })
  .then(() => {
    return cordovaPluginAdder({
      pluginName: 'https://github.com/tomloprod/cordova-plugin-appminimize.git',
      message: messages.DOWNLOAD_APP_MINIMIZE,
      workingPath
    });
  })
  .then(() => {
    return cordovaPluginAdder({
      pluginName:
        'https://github.com/vasani-arpit/cordova-plugin-downloadmanager',
      message: messages.DOWNLOAD_MANAGER_PLUGIN,
      workingPath
    });
  })
  .then(() => {
    const endTime = moment();
    var duration = moment.duration(endTime.diff(startTime));
    var minutesDuration = Math.floor(duration.asMinutes());
    console.log(chalk.green(`Completed in ${minutesDuration} 🎉🎉🎉 Minutes`));
  });
