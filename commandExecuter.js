const spawn = require('child-process-promise').spawn;
const styler = require('./styler');
const {
  BOLD_WITH_BG_THEME,
  NORMAL_WITH_BG_THEME
} = require('./constants').themes;
const { log } = console;

module.exports = function(options) {
  const { command, commandArgs, cwd, message, category } = options;
  /** log information about the beginning of the step */
  const styledCategoryMessage = styler(category, BOLD_WITH_BG_THEME);
  const styledFinishMessage = styler(message, NORMAL_WITH_BG_THEME);
  log(`${styledCategoryMessage} ${styledFinishMessage}`);

  /** spawn the new terminal and print any information streamed from the terminal */
  const promise = spawn(command, commandArgs, { cwd });
  const { childProcess } = promise;
  childProcess.stdout.on('data', data => {
    const message = data.toString();
    log(styler(message));
  });
  childProcess.stderr.on('data', function(data) {
    const message = data.toString();
    log(styler(message));
  });
  return promise.then(function() {}).catch(function() {
    // console.log(err)
    /** we do not print anything here. the errors coming from here are not detailed enough to be of value.
     *  need more investigation of the Error object.
     */
  });
};
