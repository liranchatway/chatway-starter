const commandExecuter = require('./commandExecuter');

module.exports = function(options) {
  const { platform, pluginName, workingPath, finishMessage } = options;
  const categories = require('./userMessages').categories;
  const projectPath = `./platforms/${platform}`;
  const message = `PLUGMAN - ${finishMessage}`;
  return commandExecuter({
    command: 'plugman',
    commandArgs: [
      'install',
      '--platform',
      platform,
      '--project',
      projectPath,
      '--plugin',
      pluginName
    ],
    cwd: workingPath,
    message,
    category: categories.PLUGMAN
  });
};
