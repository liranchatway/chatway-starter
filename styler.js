const chalk = require('chalk');
const {
  BOLD_WITH_BG_THEME,
  NORMAL_WITH_BG_THEME,
  SPECIAL_COLOR_THEME,
  ERROR_THEME
} = require('./constants').themes;

module.exports = function(text, style) {
  switch (style) {
    case ERROR_THEME:
      return chalk.white.bgRed(text);
    case BOLD_WITH_BG_THEME:
      return chalk.white.bgBlue.bold(text);
    case SPECIAL_COLOR_THEME:
      return chalk.magenta(text);
    case NORMAL_WITH_BG_THEME:
      return chalk.white.bgCyan(text);
    default:
      return chalk.white(text);
  }
};
